var detail, datax, avgGcse;
$(document).ready(function() {
    detail = new Detail();
});

var Detail = function() {
    this.schoolDiv = $('#content_school');
    this.detailList = $('#content_list');
};

Detail.prototype = {
    loadSchool: function(row) {
	if (table_selector == PRIMARY_TABLE_ID) {
	    detail.loadPrimarySchool(row);
	}
	if (table_selector == SECONDARY_TABLE_ID) {
	    detail.loadSecondarySchool(row);
	}
    },

    loadPrimarySchool: function(row) {
        var list, subList;


        function addRow(list, title, value) {
            var row = $('<li><span class="content_list_name">' + title + ' </span><span>' + value + '</span></li>');
            list.append(row);
            return row;
        }

        list = this.detailList;
        list.empty();

        list.append($('<li><h2 id="content_school_name">' + row.SchoolName.value + '</h2></li>'));

        addRow(list, 'Total Pupils', row.TotPup.value);

        list.append($('<li><h3>% Of pupils achieving Level 4 or above</h2></li>'));
        subList = $('<ul />');
        list.append(subList);
        var htmlRow = addRow(subList, 'Both English and Maths', row.PEngmatTestL4p.value);
        //var color = '#00FF00';
	var imag = 'upS.png';
        if (parseFloat(row.PEngmatTestL4p.value) < 78) {
        //    color = '#FF0000';
	    imag = 'downS.png';
        }
	htmlRow.append($('<span>&nbsp;&nbsp;</span><img src="' + imag + '" />'));
        //htmlRow.append($('<span style="background: '+ color + '">&nbsp;&nbsp&nbsp;&nbsp;</span>'))
        addRow(subList, 'English', row.PEngTestL4p.value);
        addRow(subList, 'Maths', row.PMatTestL4p.value);

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Year');
        data.addColumn('number', 'Performance');
        data.addRows([
            ['2008', parseFloat(row.PEngmatTestL4p08.value)],
            ['2009', parseFloat(row.PEngmatTestL4p09.value)],
            ['2010', parseFloat(row.PEngmatTestL4p10.value)]
        ]);

        var el = $('<div />');
        var li = list.append('<li />');
        li.append(el);
        var chart = new google.visualization.LineChart(el[0]);
        chart.draw(data, {width: 380, height: 240, title: 'Level 4 Performance', legend: 'none'});
    },

    loadSecondarySchool: function(row) {
        var list, subList;


        function addRow(list, title, value) {
            var row = $('<li><span class="content_list_name">' + title + ' </span><span>' + value + '</span></li>');
            list.append(row);
            return row;
        }

        list = this.detailList;
        list.empty();

        list.append($('<li><h2 id="content_school_name">' + row.SchoolName.value + '</h2></li>'));

        addRow(list, 'Total Pupils', row.TotPup.value);

        list.append($('<li><h3>% Of pupils achieving GCSE grades A* - C</h2></li>'));
        subList = $('<ul />');
        list.append(subList);
        var htmlRow = addRow(subList, 'Overall', row.P5AC.value);
        //var color = '#00FF00';
	var imag = 'upS.png';
        if (parseFloat(row.P5AC.value) < 78) {
        //    color = '#FF0000';
	    imag = 'downS.png';
        }
	htmlRow.append($('<span>&nbsp;&nbsp;</span><img src="' + imag + '" />'));
        //htmlRow.append($('<span style="background: '+ color + '">&nbsp;&nbsp&nbsp;&nbsp;</span>'))
        addRow(subList, 'Language', row.PMFLLev2.value);
        addRow(subList, 'English + Maths', row.PACEMGcse.value);

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Year');
        data.addColumn('number', 'Performance');
        data.addRows([
            ['2007', parseFloat(row.P5ACinclEM07.value)],
            ['2008', parseFloat(row.P5ACinclEM08.value)],
            ['2009', parseFloat(row.P5ACinclEM09.value)],
            ['2010', parseFloat(row.P5ACinclEM10.value)]
        ]);

        var el = $('<div />');
        var li = list.append('<li />');
        li.append(el);
        var chart = new google.visualization.LineChart(el[0]);
        chart.draw(data, {width: 380, height: 240, title: 'GCSE Performance', legend: 'none'});
    },

    setStats: function() {
	if (table_selector == PRIMARY_TABLE_ID) {
	    detail.setPrimaryStats();
	}
	if (table_selector == SECONDARY_TABLE_ID) {
	    detail.setSecondaryStats();
	}
    },

    displayPrimaryStats: function(response) {
	data = response.getDataTable();
		
	$('#best_selected').html('<option value="2"> Total Population</option><option value="3"> % Level 4 in English</option><option value="4"> % Level 4 in Maths</option><option value="5"> % Level 4 in English and Maths</option><option value="6"> Average point score</option><option value="7"> Truancy</option>');
	detail.setPrimaryStats();
	detail.setPrimaryBest();
    },

    setPrimaryStats: function() {
        if (data) {
            var selection =  parseInt($('#best_selected').val());
            var new_view = new google.visualization.DataView(data);
            var list = $('#rank_list');
            list.empty();

            var finalSorted = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, selection));

                if (!isNaN(percent)) {
                    finalSorted.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)}, 
				      TotPup: {value: new_view.getValue(i, 2)}, PEngTestL4p: {value: new_view.getValue(i, 3)},
				      PMatTestL4p: {value: new_view.getValue(i, 4)}, PEngmatTestL4p: {value: new_view.getValue(i, 5)},
				      ApsEngmatTest: {value: new_view.getValue(i, 6)}, PEngmatTestL4p08: {value: new_view.getValue(i, 8)}, 
				      PEngmatTestL4p09: {value: new_view.getValue(i, 9)}, PEngmatTestL4p10: {value: new_view.getValue(i, 10)}});
                }

            }

            finalSorted.sort(function(a, b) {
                return a.num - b.num;
            });

            if (selection != 7) {
                finalSorted.reverse();
            }

            var postFix = '&#37;'; // Percent sign
            if (selection == 2 || selection == 6) {
                postFix = '';
            }
            for (var i = 0; i < Math.min(10, finalSorted.length); i++) {
                var html = $('<li style="cursor:pointer"><span class="content_list_name">' + (i + 1) + ". " + finalSorted[i].name +' </span><br/><span>' + finalSorted[i].num + postFix + '</span></li>');
		if (i == 0)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });

		if (i == 1)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[1].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[1].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[1]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 2)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[2].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[2].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[2]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 3)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[3].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[3].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[3]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 4)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[4].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[4].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[4]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 5)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[5].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[5].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[5]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 6)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[6].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[6].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[6]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 7)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[7].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[7].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[7]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 8)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[8].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[8].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[8]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 9)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[9].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[9].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[9]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
                list.append(html);
		
            }
        }
    },

    setPrimaryBest: function() {
	if (data) {
	    list = $('#best_list');
	    list.empty();
	    var new_view = new google.visualization.DataView(data);
	    // High Pup
            var bestPup = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 2));

                if (!isNaN(percent)) {
                    bestPup.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)}, 
				      TotPup: {value: new_view.getValue(i, 2)}, PEngTestL4p: {value: new_view.getValue(i, 3)},
				      PMatTestL4p: {value: new_view.getValue(i, 4)}, PEngmatTestL4p: {value: new_view.getValue(i, 5)},
				      ApsEngmatTest: {value: new_view.getValue(i, 6)}, PEngmatTestL4p08: {value: new_view.getValue(i, 8)}, 
				      PEngmatTestL4p09: {value: new_view.getValue(i, 9)}, PEngmatTestL4p10: {value: new_view.getValue(i, 10)}});
                }
            }

            bestPup.sort(function(a, b) {
                return a.num - b.num;
            });
	    bestPup.reverse();
	    var html1 = $('<li style="cursor:pointer"><span class="content_list_name">' + "Most Pupils:" +' </span><br/><span>' + bestPup[0].name + '</span></li>');

                    html1.click(function() {
			geocoder.geocode({
                            'address': bestPup[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: bestPup[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(bestPup[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });

	    list.append(html1);
	    // % Level 4 Eng
            var bestLevel4Eng = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 3));

                if (!isNaN(percent)) {
                    bestLevel4Eng.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)}, 
				      TotPup: {value: new_view.getValue(i, 2)}, PEngTestL4p: {value: new_view.getValue(i, 3)},
				      PMatTestL4p: {value: new_view.getValue(i, 4)}, PEngmatTestL4p: {value: new_view.getValue(i, 5)},
				      ApsEngmatTest: {value: new_view.getValue(i, 6)}, PEngmatTestL4p08: {value: new_view.getValue(i, 8)}, 
				      PEngmatTestL4p09: {value: new_view.getValue(i, 9)}, PEngmatTestL4p10: {value: new_view.getValue(i, 10)}});
                }
            }

            bestLevel4Eng.sort(function(a, b) {
                return a.num - b.num;
            });
	    bestLevel4Eng.reverse();
	    
	    var html2 = $('<li style="cursor:pointer"><span class="content_list_name">' + "Highest % level 4 English:" +' </span><br/><span>' + bestLevel4Eng[0].name + '</span></li>');
                    html2.click(function() {
			geocoder.geocode({
                            'address': bestLevel4Eng[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: bestLevel4Eng[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(bestLevel4Eng[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
	    
	    list.append(html2);
	    // % Level 4 Maths
            var level4Maths = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 4));

                if (!isNaN(percent)) {
                    level4Maths.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)}, 
				      TotPup: {value: new_view.getValue(i, 2)}, PEngTestL4p: {value: new_view.getValue(i, 3)},
				      PMatTestL4p: {value: new_view.getValue(i, 4)}, PEngmatTestL4p: {value: new_view.getValue(i, 5)},
				      ApsEngmatTest: {value: new_view.getValue(i, 6)}, PEngmatTestL4p08: {value: new_view.getValue(i, 8)}, 
				      PEngmatTestL4p09: {value: new_view.getValue(i, 9)}, PEngmatTestL4p10: {value: new_view.getValue(i, 10)}});
                }
            }

            level4Maths.sort(function(a, b) {
                return a.num - b.num;
            });
	    level4Maths.reverse();
	    var html3 = $('<li style="cursor:pointer"><span class="content_list_name">' + "Highest % level 4 Maths:" +' </span><br/><span>' + level4Maths[0].name + '</span></li>');

                    html3.click(function() {
			geocoder.geocode({
                            'address': level4Maths[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: level4Maths[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(level4Maths[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
	    list.append(html3);
	    // % Level 4 Eng AND Maths
            var level4Both = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 5));

                if (!isNaN(percent)) {
                    level4Both.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)}, 
				      TotPup: {value: new_view.getValue(i, 2)}, PEngTestL4p: {value: new_view.getValue(i, 3)},
				      PMatTestL4p: {value: new_view.getValue(i, 4)}, PEngmatTestL4p: {value: new_view.getValue(i, 5)},
				      ApsEngmatTest: {value: new_view.getValue(i, 6)}, PEngmatTestL4p08: {value: new_view.getValue(i, 8)}, 
				      PEngmatTestL4p09: {value: new_view.getValue(i, 9)}, PEngmatTestL4p10: {value: new_view.getValue(i, 10)}});
                }
            }

            level4Both.sort(function(a, b) {
                return a.num - b.num;
            });
	    level4Both.reverse();
	    var html4 = $('<li style="cursor:pointer"><span class="content_list_name">' + "Highest % level 4 English and Maths:" +' </span><br/><span>' + level4Both[0].name + '</span></li>');

                    html4.click(function() {
			geocoder.geocode({
                            'address': level4Both[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: level4Both[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(level4Both[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
	    
	    list.append(html4);
	    // Highest average
            var highestAvg = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 6));

                if (!isNaN(percent)) {
                    highestAvg.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)}, 
				      TotPup: {value: new_view.getValue(i, 2)}, PEngTestL4p: {value: new_view.getValue(i, 3)},
				      PMatTestL4p: {value: new_view.getValue(i, 4)}, PEngmatTestL4p: {value: new_view.getValue(i, 5)},
				      ApsEngmatTest: {value: new_view.getValue(i, 6)}, PEngmatTestL4p08: {value: new_view.getValue(i, 8)}, 
				      PEngmatTestL4p09: {value: new_view.getValue(i, 9)}, PEngmatTestL4p10: {value: new_view.getValue(i, 10)}});
                }
            }

            highestAvg.sort(function(a, b) {
                return a.num - b.num;
            });
	    highestAvg.reverse();
	    var html5 = $('<li style="cursor:pointer"><span class="content_list_name">' + "Highest average English and Maths:" +' </span><br/><span>' + highestAvg[0].name + '</span></li>');

                    html5.click(function() {
			geocoder.geocode({
                            'address': highestAvg[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: highestAvg[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(highestAvg[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
	    list.append(html5);
	    // Low Abs
            var lowestAbsPrim = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 7));

                if (!isNaN(percent)) {
                    lowestAbsPrim.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)}, 
				      TotPup: {value: new_view.getValue(i, 2)}, PEngTestL4p: {value: new_view.getValue(i, 3)},
				      PMatTestL4p: {value: new_view.getValue(i, 4)}, PEngmatTestL4p: {value: new_view.getValue(i, 5)},
				      ApsEngmatTest: {value: new_view.getValue(i, 6)}, PEngmatTestL4p08: {value: new_view.getValue(i, 8)}, 
				      PEngmatTestL4p09: {value: new_view.getValue(i, 9)}, PEngmatTestL4p10: {value: new_view.getValue(i, 10)}});
                }
            }

            lowestAbsPrim.sort(function(a, b) {
                return a.num - b.num;
            });
	    html6 = $('<li style="cursor:pointer"><span class="content_list_name">' + "Lowest truancy:" +' </span><br/><span>' + lowestAbsPrim[0].name + '</span></li>');
                    html6.click(function() {
			geocoder.geocode({
                            'address': lowestAbsPrim[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: lowestAbsPrim[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(lowestAbsPrim[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
	    list.append(html6);

	}
    },

    displaySecondaryStats: function(response) {
	data = response.getDataTable();
	
	$('#best_selected').html('<option value="2"> Total Population</option><option value="3"> % With 5 A*-C</option><option value="4"> % With 5 A*-C incl Maths and English</option><option value="5"> % With 2 A*-C in Sciences</option><option value="6"> Truancy</option>');
	detail.setSecondaryStats();
	detail.setSecondaryBest();
    },

    setSecondaryStats: function() {
        if (data) {
            var selection =  parseInt($('#best_selected').val());
            var new_view = new google.visualization.DataView(data);
            var list = $('#rank_list');
            list.empty();

            var finalSorted = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, selection));

                if (!isNaN(percent)) {
                    finalSorted.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)},
				      TotPup: {value: new_view.getValue(i, 2)}, P5AC: {value: new_view.getValue(i, 3)}, P5ACinclEM: {value: new_view.getValue(i, 4)},
				      P2ACScience: {value: new_view.getValue(i, 5)}, P5ACinclEM07: {value: new_view.getValue(i, 7)}, P5ACinclEM08: {value: new_view.getValue(i, 8)}, P5ACinclEM09: {value: new_view.getValue(i, 9)}, P5ACinclEM10: {value: new_view.getValue(i, 10)}, PMFLLev2: {value: new_view.getValue(i, 11)},
				      PACEMGcse: {value: new_view.getValue(i, 12)}});
                }

            }

            finalSorted.sort(function(a, b) {
                return a.num - b.num;
            });

            if (selection != 6) {
                finalSorted.reverse();
            }

            var postFix = '&#37;'; // Percent sign
            if (selection == 2) {
                postFix = '';
            }

            for (var i = 0; i < Math.min(10, finalSorted.length); i++) {
                var html = $('<li style="cursor:pointer"><span class="content_list_name">' + (i + 1) + ". " + finalSorted[i].name +' </span><br/><span>' + finalSorted[i].num + postFix + '</span></li>');
		if (i == 0)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });

		if (i == 1)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[1].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[1].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[1]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 2)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[2].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[2].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[2]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 3)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[3].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[3].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[3]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 4)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[4].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[4].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[4]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 5)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[5].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[5].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[5]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 6)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[6].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[6].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[6]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 7)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[7].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[7].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[7]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 8)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[8].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[8].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[8]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
		if (i == 9)
                    html.click(function() {
			geocoder.geocode({
                            'address': finalSorted[9].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: finalSorted[9].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(finalSorted[9]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
                list.append(html);

            }
        }
    },

    setSecondaryBest: function() {
	if (data) {
	    list = $('#best_list');
	    list.empty();
	    var new_view = new google.visualization.DataView(data);
	    // High Pup
            var highPupSec = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 2));

                if (!isNaN(percent)) {
                    highPupSec.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)},
				      TotPup: {value: new_view.getValue(i, 2)}, P5AC: {value: new_view.getValue(i, 3)}, P5ACinclEM: {value: new_view.getValue(i, 4)},
				      P2ACScience: {value: new_view.getValue(i, 5)}, P5ACinclEM07: {value: new_view.getValue(i, 7)}, P5ACinclEM08: {value: new_view.getValue(i, 8)}, P5ACinclEM09: {value: new_view.getValue(i, 9)}, P5ACinclEM10: {value: new_view.getValue(i, 10)}, PMFLLev2: {value: new_view.getValue(i, 11)},
				      PACEMGcse: {value: new_view.getValue(i, 12)}});
                }
            }

            highPupSec.sort(function(a, b) {
                return a.num - b.num;
            });
	    highPupSec.reverse();
	    if (highPupSec.length > 0) {
	    var html11 = $('<li style="cursor:pointer"><span class="content_list_name">' + "Most Pupils:" +' </span><br/><span>' + highPupSec[0].name + '</span></li>');

                    html11.click(function() {
			geocoder.geocode({
                            'address': highPupSec[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: highPupSec[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(highPupSec[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
	    list.append(html11);
	    }
	    // High ACHighest % with 5 A*-C:"
            var acHighest5AC = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 3));

                if (!isNaN(percent)) {
                    acHighest5AC.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)},
				      TotPup: {value: new_view.getValue(i, 2)}, P5AC: {value: new_view.getValue(i, 3)}, P5ACinclEM: {value: new_view.getValue(i, 4)},
				      P2ACScience: {value: new_view.getValue(i, 5)}, P5ACinclEM07: {value: new_view.getValue(i, 7)}, P5ACinclEM08: {value: new_view.getValue(i, 8)}, P5ACinclEM09: {value: new_view.getValue(i, 9)}, P5ACinclEM10: {value: new_view.getValue(i, 10)}, PMFLLev2: {value: new_view.getValue(i, 11)},
				      PACEMGcse: {value: new_view.getValue(i, 12)}});
                }
            }

            acHighest5AC.sort(function(a, b) {
                return a.num - b.num;
            });
	    acHighest5AC.reverse();
	    if (acHighest5AC.length >0){
        var total= 0;
        for (var i = 0; i < acHighest5AC.length; i++) {
            if (acHighest5AC[i].num > 1) {
                total += acHighest5AC[i].num;
            }
        }
        avgGcse = total / acHighest5AC.length;
	    html22 = $('<li style="cursor:pointer"><span class="content_list_name">' + "Highest % with 5 A*-C:" +' </span><br/><span>' + acHighest5AC[0].name + '</span></li>');
                    html22.click(function() {
			geocoder.geocode({
                            'address': acHighest5AC[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: acHighest5AC[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(acHighest5AC[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
	    list.append(html22);
	    }
	    // High ACHighest % with 5 A*-C:"
            var highestACwithMandE = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 4));

                if (!isNaN(percent)) {
                    highestACwithMandE.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)},
				      TotPup: {value: new_view.getValue(i, 2)}, P5AC: {value: new_view.getValue(i, 3)}, P5ACinclEM: {value: new_view.getValue(i, 4)},
				      P2ACScience: {value: new_view.getValue(i, 5)}, P5ACinclEM07: {value: new_view.getValue(i, 7)}, P5ACinclEM08: {value: new_view.getValue(i, 8)}, P5ACinclEM09: {value: new_view.getValue(i, 9)}, P5ACinclEM10: {value: new_view.getValue(i, 10)}, PMFLLev2: {value: new_view.getValue(i, 11)},
				      PACEMGcse: {value: new_view.getValue(i, 12)}});
                }
            }

            highestACwithMandE.sort(function(a, b) {
                return a.num - b.num;
            });
	    highestACwithMandE.reverse();
	    if (highestACwithMandE.length > 0) {
	    var html33 = $('<li style="cursor:pointer"><span class="content_list_name">' + "Highest % with 5 A*-C with A*-C in Maths and English:" +' </span><br/><span>' + highestACwithMandE[0].name + '</span></li>');
                    html33.click(function() {
			geocoder.geocode({
                            'address': highestACwithMandE[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: highestACwithMandE[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(highestACwithMandE[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
	    list.append(html33);
	    }
	    // High ACHighest % with 5 A*-C:"
            var highACSci = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 5));

                if (!isNaN(percent)) {
                    highACSci.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)},
				      TotPup: {value: new_view.getValue(i, 2)}, P5AC: {value: new_view.getValue(i, 3)}, P5ACinclEM: {value: new_view.getValue(i, 4)},
				      P2ACScience: {value: new_view.getValue(i, 5)}, P5ACinclEM07: {value: new_view.getValue(i, 7)}, P5ACinclEM08: {value: new_view.getValue(i, 8)}, P5ACinclEM09: {value: new_view.getValue(i, 9)}, P5ACinclEM10: {value: new_view.getValue(i, 10)}, PMFLLev2: {value: new_view.getValue(i, 11)},
				      PACEMGcse: {value: new_view.getValue(i, 12)}});
                }
            }

            highACSci.sort(function(a, b) {
                return a.num - b.num;
            });
	    highACSci.reverse();
	    if (highACSci.length > 0) {
	    html44=$('<li style="cursor:pointer"><span class="content_list_name">' + "Highest % with 2 A*-C in Sciences:" +' </span><br/><span>' + highACSci[0].name + '</span></li>');

                    html44.click(function() {
			geocoder.geocode({
                            'address': highACSci[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: highACSci[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(highACSci[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
	    list.append(html44);
	    }
	    // Low Abs
            var lowAbsSec = [];
            for (var i = 0; i < new_view.getNumberOfRows(); i++) {
                var percent = parseFloat(new_view.getValue(i, 6));

                if (!isNaN(percent)) {
                    lowAbsSec.push({name: new_view.getValue(i, 1), num: percent, address: new_view.getValue(i, 0), SchoolName: {value: new_view.getValue(i, 1)},
				      TotPup: {value: new_view.getValue(i, 2)}, P5AC: {value: new_view.getValue(i, 3)}, P5ACinclEM: {value: new_view.getValue(i, 4)},
				      P2ACScience: {value: new_view.getValue(i, 5)}, P5ACinclEM07: {value: new_view.getValue(i, 7)}, P5ACinclEM08: {value: new_view.getValue(i, 8)}, P5ACinclEM09: {value: new_view.getValue(i, 9)}, P5ACinclEM10: {value: new_view.getValue(i, 10)}, PMFLLev2: {value: new_view.getValue(i, 11)},
				      PACEMGcse: {value: new_view.getValue(i, 12)}});
                }
            }

            lowAbsSec.sort(function(a, b) {
                return a.num - b.num;
            });
	    if (lowAbsSec.length > 0) {
	    html55 = $('<li style="cursor:pointer"><span class="content_list_name">' + "Lowest truancy:" +' </span><br/><span>' + lowAbsSec[0].name + '</span></li>');

                    html55.click(function() {
			geocoder.geocode({
                            'address': lowAbsSec[0].address
			}, function(results, status) {
                            if (results[0]) {
				var location = results[0].geometry.location;
				
				if (info)
                                    info.close();
				
				info = new google.maps.InfoWindow({
                                    content: lowAbsSec[0].name,
                                    position: location
				});
				info.open(map);
				detail.loadSchool(lowAbsSec[0]);
				tabSwitch('tab_school', 'content_school');
				map.setCenter(location);
				loadLayer();
                            }
			});
                    });
	    list.append(html55);
	    }
	}
    }
};
