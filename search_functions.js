function searchMap() {
    geocoder.geocode({
	    'address': $('#searchField').val()
    }, function(results, status) {
	if (results[0]) {
	    var location = results[0].geometry.location;
            marker.setPosition(location);
            map.setCenter(location);
	    //console.log($('input:radio[name=schoolLevel]:checked'));
	    if ($('input:radio[name=schoolLevel]:checked').val() == 'primary')
		table_selector = PRIMARY_TABLE_ID;
	    if ($('input:radio[name=schoolLevel]:checked').val() == 'secondary')
		table_selector = SECONDARY_TABLE_ID;
	    loadLayer();
	}
    });
}
