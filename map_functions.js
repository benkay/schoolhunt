var PRIMARY_TABLE_ID = '1777583',
    SECONDARY_TABLE_ID = '1777484';

var layer, map, info, geocoder, marker, table_selector, avgGcse;
//var uk, Options;
function initializeMap() {
    var uk, Options;

    uk = new google.maps.LatLng(51.500000, -0.116667);
    Options = {
    	zoom: 12,
    	center: uk,
    	mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map"), Options);
    geocoder = new google.maps.Geocoder();
    marker = new google.maps.Marker({
	    map: map
    });
    google.maps.event.addListener(map, "idle", loadLayer);
    $('#searchForm').submit(function() {
        searchMap();
        return false;
    });

    table_selector = SECONDARY_TABLE_ID;
    loadLayer();

    $('#best_selected').change(detail.setStats);
}

function loadMap() {
    var script = $('<script src="http://maps.google.com/maps/api/js?sensor=false&callback=initializeMap&region=GB">');
    $('body').append(script);
}

function loadLayer() {
    // map hasn't loaded
    if (!map.getBounds()) {
        return;
    }

    // Remove the old layer
    if (layer) {
        layer.setMap(null);
    }

    layer = new google.maps.FusionTablesLayer({
        query: {
            select: 'FullAddress',
            from: table_selector,
            where: buildWhereQuery()
        }
    });

    layer.setOptions({
	    suppressInfoWindows: true
    });

    layer.setMap(map);

    google.maps.event.addListener(layer, "click", function(event) {
        if (info)
            info.close();

        info = new google.maps.InfoWindow({
            content: event.row['SchoolName'].value,
            position: event.latLng
        });
        info.open(map);
        detail.loadSchool(event.row);
        tabSwitch('tab_school', 'content_school');
    });

    if (table_selector == PRIMARY_TABLE_ID) {
        var query = new google.visualization.Query('http://www.google.com/fusiontables/gvizdata?tq=' + encodeURIComponent("SELECT FullAddress, SchoolName, TotPup, PEngTestL4p, PMatTestL4p, PEngmatTestL4p, ApsEngmatTest, PAbsUa, PEngmatTestL4p08, PEngmatTestL4p09, PEngmatTestL4p10 FROM " + table_selector + " WHERE " + buildWhereQuery()));
        query.send(detail.displayPrimaryStats);
    }
    if (table_selector == SECONDARY_TABLE_ID) {
        var query = new google.visualization.Query('http://www.google.com/fusiontables/gvizdata?tq=' + encodeURIComponent("SELECT FullAddress, SchoolName, TotPup, P5AC, P5ACinclEM, P2ACScience, PAbsTot, P5ACinclEM07, P5ACinclEM08, P5ACinclEM09, P5ACinclEM10, PMFLLev2, PACEMGcse FROM " + table_selector + " WHERE " + buildWhereQuery()));
        query.send(detail.displaySecondaryStats);
    }
}

function buildWhereQuery(options) {
    var lowerLeft, upperRight, lowerLeftLat, lowerLeftLong, upperRightLat, upperRightLong, where, recType;

    options = options || {};

    select = 'FullAddress';
    from = table_selector;
    where = '';

    lowerLeft = map.getBounds().getSouthWest();
    upperRight = map.getBounds().getNorthEast();

    lowerLeftLat = lowerLeft.lat();
    lowerLeftLong = lowerLeft.lng();

    upperRightLat = upperRight.lat();
    upperRightLong = upperRight.lng();

    where += 'ST_INTERSECTS(FullAddress, RECTANGLE(LATLNG(' + lowerLeftLat + ', ' + lowerLeftLong + '), LATLNG(' + upperRightLat + ', ' + upperRightLong + ')))';
    
    if (options.special) {
        recType = '2';
    } else {
        recType = '1';
    }

    // EVERYTHING IS BROKEN
    //where += " AND RecType = " + recType;

    if (options.gender === 'boys') {
        where += " AND EntryGender = 'BOYS'";
    } else if (options.gender === 'girls') {
        where += " AND EntryGender = 'GIRLS'";
    }

//    where += " AND FlagClosed='0'";

    //console.log(where);
    return where;
}
